<?php

namespace Unit4\DataMapper;

use Unit4\Connection;

/**
 * BookMapper.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
class BookMapper 
{
    private $connection;
    /**
     * @var \ReflectionProperty[]
     */
    private $reflections;

    /**
     * Constructor.
     *
     * @param Connection $connection The connection
     * @param string     $className  The book full qualified class name
     */
    public function __construct(Connection $connection, $className = 'Unit4\DataMapper\Book')
    {
        $this->connection = $connection;
        $this->reflections = array();

        $fieldToPropertyMap = array(
            'id' => 'id',
            'title' => 'title',
            'description' => 'description',
            'author_id' => 'authorId',
            'status' => 'status',
            'date' => 'date',
        );

        foreach ($fieldToPropertyMap as $field => $property) {
            $this->reflections[$field] = $this->createReflection($property, $className);
        }
    }

    /**
     * Finds a book by id.
     *
     * @param int $id An identifier
     *
     * @return null|Book
     */
    public function find($id)
    {
        //Retrieve the data from db
        $row = $this->connection->fetchAssoc(
            'SELECT title, description, author_id, status, date FROM Books WHERE id = :id',
            array('id' => $id)
        );

        if (!count($row)) {
            return null;
        }

        $book = new Book();

        //Map data into model.
        $this->setValue($book, 'id', $id);
        $this->setValue($book, 'title', $row['title']);
        $this->setValue($book, 'description', $row['description']);
        $this->setValue($book, 'author_id', $row['author_id']);
        $this->setValue($book, 'status', $row['status']);
        $this->setValue($book, 'date', \DateTime::createFromFormat('Y-m-d H:i:s', $row['date']));

        return $book;
    }

    /**
     * Saves the book.
     *
     * @param Book $book A book
     *
     * @return bool
     */
    public function save(Book $book)
    {
        $params = $this->createParams($book);
        $isNew = $params['id'] === null;

        if ($isNew) {
            $query = 'INSERT INTO Books '
                . '(title, description, author_id, status, date) '
                . 'VALUES (:title, :description, :author_id, :status, :date)';
        } else {
            $query = 'UPDATE Books '
                . 'SET title = :title, description = :desc, author_id = :author_id, status = :status, date = :date '
                . 'WHERE id = :id';
        }

        $result = $this->connection->exec($query, $params);

        if ($result > 0 && $isNew) {
            $this->setValue($book, 'id', $this->connection->getLastInsertedId());
        }

        return $result > 0;
    }

    /**
     * Removes the book.
     *
     * @param Book $book A book
     *
     * @return bool
     */
    public function remove(Book $book)
    {
        $id = $this->getValue($book, 'id');

        if ($id === null) {
            return true;
        }

        $result = $this->connection->exec('DELETE FROM Books WHERE id = :id', array('id' => $id));

        return $result > 0;
    }

    /**
     * Extracts all property values from book model.
     *
     * @param Book $book A book
     *
     * @return array
     */
    private function createParams(Book $book)
    {
        $param = array();

        foreach ($this->reflections as $field => $reflection) {
            $param[$field] = $reflection->getValue($book);
        }

        return $param;
    }

    /**
     * Sets value to the book property.
     *
     * @param Book   $book  A book
     * @param string $field A field name
     * @param mixed  $value A field value
     */
    private function setValue(Book $book, $field, $value)
    {
        if (isset($this->reflections[$field])) {
            $this->reflections[$field]->setValue($book, $value);
        }
    }

    /**
     * Gets value of the book's property.
     *
     * @param Book   $book  A book
     * @param string $field A field name
     *
     * @return mixed|null
     */
    private function getValue(Book $book, $field)
    {
        if (isset($this->reflections[$field])) {
            return $this->reflections[$field]->getValue($book);
        }

        return null;
    }

    /**
     * Creates reflection for class property.
     *
     * @param string $property  A property name
     * @param string $className A class name
     *
     * @return \ReflectionProperty
     */
    private function createReflection($property, $className)
    {
        $reflection = new \ReflectionProperty($className, $property);
        $reflection->setAccessible(true);

        return $reflection;
    }
}
