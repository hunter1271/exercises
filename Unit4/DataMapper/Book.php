<?php

namespace Unit4\DataMapper;

/**
 * Book.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
class Book 
{
    private $id;
    private $title;
    private $description;
    private $authorId;
    private $status;
    private $date;

    /**
     * Gets the identifier.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the title.
     *
     * @param string $title A title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gets the title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the description.
     *
     * @param string $description A description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Gets the description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the author identifier.
     *
     * @param int $authorId An author identifier
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * Gets the author identifier.
     *
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Sets the status.
     *
     * @param int $status A status value
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Gets the status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the date.
     *
     * @param \DateTime $date A date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Gets the date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
