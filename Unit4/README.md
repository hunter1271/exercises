# Реализация ActiveRecord и DataMapper для модели Book. #

Класс [Connection](https://bitbucket.org/hunter1271/exercises/src/master/Unit4/Connection.php) - условно представляет соединение с БД. Будет использоваться в примерах.

## ActiveRecord ##
Реализация модели [Book](https://bitbucket.org/hunter1271/exercises/src/master/Unit4/ActiveRecord/Book.php) для шаблона ActiveRecord представляет собой класс объединяющий в себе данные модели и методы управления ими (чтение, изменение).

## DataMapper ##
При реализации шаблона DataMapper кроме самой модели [Book](https://bitbucket.org/hunter1271/exercises/src/master/Unit4/DataMapper/Book.php) требуется реализовать класс отвечающий за чтение и запись данных модели в БД - [BookMapper](https://bitbucket.org/hunter1271/exercises/src/master/Unit4/DataMapper/BookMapper.php). В этом случае модель не хранит в себе методов чтения и записи собственных данных.