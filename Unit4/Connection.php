<?php

namespace Unit4;

/**
 * Connection.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
class Connection 
{
    private static $instance;

    /**
     * Private constructor.
     */
    private function __construct()
    {
    }

    /**
     * Gets instance.
     *
     * @return Connection
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Execute an SQL statement and fetch first row from query results as associated array.
     *
     * Not implemented yet.
     *
     * @param string $stmt   A query string
     * @param array  $params An array of params
     *
     * @return array
     */
    public function fetchAssoc($stmt, array $params = array())
    {
    }

    /**
     * Execute an SQL statement and fetch all rows from query results as associated arrays.
     *
     * Not implemented yet.
     *
     * @param string $stmt   A query string
     * @param array  $params An array of params
     *
     * @return array
     */
    public function fetchAll($stmt, array $params = array())
    {
    }

    /**
     * Execute an SQL statement and return the number of affected rows.
     *
     * Not implemented yet.
     *
     * @param string $stmt   A query string
     * @param array  $params An array of params
     *
     * @return array
     */
    public function exec($stmt, array $params = array())
    {
    }

    /**
     * Gets the identifier of last inserted row.
     *
     * Not implemented yet.
     *
     * @return int
     */
    public function getLastInsertedId()
    {
    }
}
