<?php

namespace Unit4\ActiveRecord;

use Unit4\Connection;

/**
 * Book.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
class Book 
{
    private $connection;

    private $id;
    private $title;
    private $description;
    private $authorId;
    private $status;
    private $date;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->connection = Connection::getInstance();
    }

    /**
     * Finds book by identifier.
     *
     * @param int $id An identifier
     *
     * @return bool
     */
    public function find($id)
    {
        //Retrieve the data from db
        $row = $this->connection->fetchAssoc(
            'SELECT title, description, author_id, status, date FROM Books WHERE id = :id',
            array('id' => $id)
        );

        if (!count($row)) {
            return false;
        }

        //Map data into model.
        $this->id = $id;
        $this->title = $row['title'];
        $this->description = $row['description'];
        $this->authorId = $row['author_id'];
        $this->status = $row['status'];
        $this->date = \DateTime::createFromFormat('Y-m-d H:i:s', $row['date']);

        return true;
    }

    /**
     * Saves the book.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->id === null) {
            $query = 'INSERT INTO Books '
                . '(title, description, author_id, status, date) '
                . 'VALUES (:title, :description, :author_id, :status, :date)';
        } else {
            $query = 'UPDATE Books '
                . 'SET title = :title, description = :desc, author_id = :author_id, status = :status, date = :date '
                . 'WHERE id = :id';
        }

        $result = $this->connection->exec($query, array(
            'id' => $this->id,
            'title' => $this->title,
            'desc' => $this->description,
            'author_id' => $this->authorId,
            'status' => $this->status,
            'date' => $this->date->format('Y-m-d H:i:s')
        ));

        //Update id for new books.
        if ($this->id === null) {
            $this->id = $this->connection->getLastInsertedId();
        }

        return $result > 0;
    }

    /**
     * Deletes the book.
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->id === null) {
            return true;
        }

        $result = $this->connection->exec('DELETE FROM Books WHERE id = :id', array('id' => $this->id));

        return $result > 0;
    }

    /**
     * Gets the identifier.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the title.
     *
     * @param string $title A title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gets the title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the description.
     *
     * @param string $description A description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Gets the description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the author identifier.
     *
     * @param int $authorId An author identifier
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * Gets the author identifier.
     *
     * @return int
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Sets the status.
     *
     * @param int $status A status value
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Gets the status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the date.
     *
     * @param \DateTime $date A date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Gets the date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
