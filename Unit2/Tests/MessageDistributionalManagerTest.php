<?php

namespace Unit2\Tests;

use Unit2\MessageDistributionalManager;

/**
 * MessageDistributionalManagerTest.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
class MessageDistributionalManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $registry;

    /**
     * @var MessageDistributionalManager
     */
    private $manager;

    public static function setUpBeforeClass()
    {
        //Add simple autoloader.
        spl_autoload_register(function ($class) {
                $prefix = 'Unit2\\';

                if (strpos($class ,$prefix) === 0) {
                    require  __DIR__ . '/../' . substr($class, strlen($prefix)) . '.php';
                }
            });
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid message given.
     */
    public function testSaveMessageWithoutFirmIdShouldThrowException()
    {
        $message = array('subject' => 'foo', 'body' => 'bar', 'from' => 'me', 'to' => 'you');

        $this->registry->expects($this->never())
            ->method('has');

        $this->registry->expects($this->never())
            ->method('get');

        $this->manager->save($message);
    }

    /**
     * @expectedException \UnexpectedValueException
     * @expectedExceptionMessage Unknown firm id (7) given.
     */
    public function testSaveMessageForUnknownFirmIdShouldThrowException()
    {
        $message = array('firm_id' => 7, 'subject' => '...');

        $this->registry->expects($this->once())
            ->method('has')
            ->will($this->returnValue(false));

        $this->registry->expects($this->never())
            ->method('get');

        $this->manager->save($message);
    }

    public function testSaveSeveralMessages()
    {
        $messages = array(
            $message1 = $this->createMessage(1),
            $message2 = $this->createMessage(2),
            $message3 = $this->createMessage(1),
            $message4 = $this->createMessage(3),
            $message5 = $this->createMessage(4),
            $message6 = $this->createMessage(4),
        );

        $manager1 = $this->getManagerMock();
        $manager2 = $this->getManagerMock();
        $manager3 = $this->getManagerMock();

        $manager1->expects($this->exactly(2))
            ->method('save')
            ->with($this->logicalOr($message1, $message3));

        $manager2->expects($this->once())
            ->method('save')
            ->with($this->equalTo($message2));

        $manager3->expects($this->exactly(3))
            ->method('save')
            ->with($this->logicalOr($message4, $message5, $message6));

        $this->registry->expects($this->exactly(count($messages)))
            ->method('has')
            ->with($this->logicalOr(1, 2, 3, 4))
            ->will($this->returnValue(true));

        $this->registry->expects($this->exactly(count($messages)))
            ->method('get')
            ->will($this->returnValueMap(array(
                array(1, $manager1),
                array(2, $manager2),
                array(3, $manager3),
                array(4, $manager3),
            )));

        foreach ($messages as $message) {
            $this->manager->save($message);
        }
    }

    protected function setUp()
    {
        $this->registry = $this->getMock('Unit2\\MessageManagerRegistryInterface');

        $this->manager = new MessageDistributionalManager($this->registry);
    }

    protected function tearDown()
    {
        unset($this->registry, $this->manager);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getManagerMock()
    {
        return $this->getMock('Unit2\\MessageManagerInterface');
    }

    /**
     * @param int $firmId
     *
     * @return array
     */
    private function createMessage($firmId)
    {
        return array(
            'firm_id' => $firmId,
            'subject' => 'subject_' . $firmId,
            'body' => 'body_' . $firmId,
            'from' => 'from_' . $firmId,
            'to' => 'to_' . $firmId
        );
    }
}
