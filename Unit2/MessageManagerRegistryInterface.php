<?php

namespace Unit2;

/**
 * MessageManagerRegistryInterface.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
interface MessageManagerRegistryInterface
{
    /**
     * Determines whether the registry has manager for given firm.
     *
     * @param int $firmId A firm identifier
     *
     * @return bool
     */
    public function has($firmId);

    /**
     * Gets message manager for given firm.
     *
     * @param int $firmId A firm identifier
     *
     * @return MessageManagerInterface|null
     */
    public function get($firmId);
}
