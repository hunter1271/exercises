# Запись массива сообщений в несколько баз данных.

Для реализации логики записи сообщения в разные БД в зависимости от значения `firm_id` потребуются сл. классы:

1. [MessageManagerInterface](https://bitbucket.org/hunter1271/exercises/src/master/Unit2/MessageManagerInterface.php) - интерфейс менеджера сообщений. Простая реализация содержит ссылку на соединение с БД и осуществляет запись в эту БД.

2. [MessageManagerRegistryInterface](https://bitbucket.org/hunter1271/exercises/src/master/Unit2/MessageManagerRegistryInterface.php) - интерфейс регистра менеджеров сообщений. Хранит в себе информацию о соответствии между `firm_id` и конкретным менеджером (т.е. БД).

3. [MessageDistributionalManager](https://bitbucket.org/hunter1271/exercises/src/master/Unit2/MessageDistributionalManager.php) - реализует `MessageManagerInterface` и содержит ссылку на реализацию `MessageManagerRegistryInterface`. На основе данных из регистра записывает сообщение в определенную БД.

При такой архитектуре алгоритм записи сводится к обходу массива сообщений в цикле и вызове для каждого сообщения `MessageDistributionalManager::save()`. Как это сделано в [тесте](https://bitbucket.org/hunter1271/exercises/src/master/Unit2/Tests/MessageDistributionalManagerTest.php#cl-112).