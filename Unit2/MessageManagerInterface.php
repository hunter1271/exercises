<?php

namespace Unit2;

/**
 * MessageManagerInterface.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
interface MessageManagerInterface 
{
    /**
     * Saves given message in storage.
     *
     * Получает сообщение в виде ассоциативного массива и записывает его в хранилище.
     *
     * @param array $message A message data
     */
    public function save(array $message);
}
