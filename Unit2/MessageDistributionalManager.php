<?php

namespace Unit2;

/**
 * MessageDistributionalManager.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
class MessageDistributionalManager implements MessageManagerInterface
{
    private $managerRegistry;

    /**
     * Constructor.
     *
     * @param MessageManagerRegistryInterface $managerRegistry The managers registry
     */
    public function __construct(MessageManagerRegistryInterface $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException
     */
    public function save(array $message)
    {
        if (!isset($message['firm_id'])) {
            throw new \InvalidArgumentException('Invalid message given.');
        }

        $this->getManager($message['firm_id'])->save($message);
    }

    /**
     * Gets manager for firm.
     *
     * @param int $firmId A firm identifier
     *
     * @return MessageManagerInterface
     *
     * @throws \UnexpectedValueException
     */
    private function getManager($firmId)
    {
        if ($this->managerRegistry->has($firmId)) {
            return $this->managerRegistry->get($firmId);
        }

        throw new \UnexpectedValueException(sprintf('Unknown firm id (%d) given.', $firmId));
    }
}
