<?php

require 'InSequence.php';

/**
 * InSequenceTest.
 *
 * @author Ural Davletshin <u.davletshin@biplane.ru>
 */
class InSequenceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param int  $needle         A searched number
     * @param int  $sequence       A sequence
     * @param bool $expectedResult A expected result
     *
     * @dataProvider sequenceProvider
     */
    public function testInSequence($needle, $sequence, $expectedResult)
    {
        $this->assertEquals($expectedResult, inSequence($needle, $sequence));

        //Test myself with native function
        $this->assertEquals($expectedResult, in_array($needle, $sequence));
    }

    public function sequenceProvider()
    {
        return array(
            array(47, range(0, 99), true),
            array(11, range(12, 50), false),
            array(101, range(0, 99), false),
            array(55, range(0, 100, 3), false),
            array(3, array(), false),
            array(7, array(7), true),
        );
    }
}
