# Поиск числа в упорядоченной последовательности. #

[Функция](https://bitbucket.org/hunter1271/exercises/src/master/Unit1/InSequence.php) определяющая принадлежность числа последовательности представляет собой реализацию алгоритма бинарного поиска или половинного деления.

[Тест](https://bitbucket.org/hunter1271/exercises/src/master/Unit1/InSequenceTest.php#cl-19) для проверки алгоритма.