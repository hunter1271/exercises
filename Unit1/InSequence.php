<?php

/**
 * Checks if a number in the sequence.
 *
 * @param int   $needle   A searched number
 * @param int[] $sequence A sequence of numbers
 *
 * @return bool
 */
function inSequence($needle, array $sequence)
{
    $length = count($sequence);
    $left = 0;
    $right = $length - 1;

    while ($left < $right) {
        if ($needle < $sequence[$left] || $needle > $sequence[$right]) {
            return false;
        }

        $middle = (int)floor($left + ($right - $left) / 2);

        if ($needle <= $sequence[$middle]) {
            $right = $middle;
        } else if ($needle > $sequence[$middle]) {
            $left = $middle + 1;
        }
    }

    return $length && $sequence[$right] === $needle;
}
