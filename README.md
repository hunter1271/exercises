# Тестовые задания

1. [Функция проверки принадлежности числа упорядоченной последовательности.](https://bitbucket.org/hunter1271/exercises/src/master/Unit1)

2. [Запись сообщений в несколько баз данных.](https://bitbucket.org/hunter1271/exercises/src/master/Unit2)

3. [Вывод книг в авторами и постраничной навигацией.](https://bitbucket.org/hunter1271/exercises/src/master/Unit3)

4. [Реализация ActiveRecord и DataMapper для модели Book.](https://bitbucket.org/hunter1271/exercises/src/master/Unit4)