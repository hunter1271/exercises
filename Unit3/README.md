# Выборка из таблиц Books и Authors. #

Запросы на [создание](https://bitbucket.org/hunter1271/exercises/src/master/Unit3/CreateTables.sql) структуры таблиц и [выборку](https://bitbucket.org/hunter1271/exercises/src/master/Unit3/Select.sql) данных.

При выборке данные фильтруются по значению поля `Books.status` и сортируются по `Books.date`. Запрос выполнится быстрее, если условия отбора и сортировки будут применены к индексам, а не к самим данным. Учитывая, что mysql одновременное может использовать только один индекс, для оптимизации выборки необходим композитный индекс на основе полей `Books.status` и `Books.date`:

```
#!sql

INDEX IDX_STATUS_DATE (status, date)
```


