SELECT b.book_id, a.name, b.title, b.date
FROM Books b LEFT JOIN Authors a ON b.author_id = a.author_id
WHERE b.status = :status
ORDER BY b.date
LIMIT :offset, :limit
